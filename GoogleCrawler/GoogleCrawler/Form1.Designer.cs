﻿namespace GoogleCrawler
{
  partial class Form1
  {
    /// <summary>
    /// 필수 디자이너 변수입니다.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// 사용 중인 모든 리소스를 정리합니다.
    /// </summary>
    /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form 디자이너에서 생성한 코드

    /// <summary>
    /// 디자이너 지원에 필요한 메서드입니다. 
    /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
    /// </summary>
    private void InitializeComponent()
    {
      this.label2 = new System.Windows.Forms.Label();
      this.Search_Tbx = new System.Windows.Forms.TextBox();
      this.Post_Rbn = new System.Windows.Forms.RadioButton();
      this.Cafe_Rbn = new System.Windows.Forms.RadioButton();
      this.News_Rbn = new System.Windows.Forms.RadioButton();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.Search_Lbx = new System.Windows.Forms.ListBox();
      this.Video_Lbx = new System.Windows.Forms.ListBox();
      this.Search_Btn = new System.Windows.Forms.Button();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.Search_Lbx2 = new System.Windows.Forms.ListBox();
      this.Video_Lbx2 = new System.Windows.Forms.ListBox();
      this.Blog_Rbn = new System.Windows.Forms.RadioButton();
      this.label9 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.Option1_Rbn = new System.Windows.Forms.RadioButton();
      this.Option2_Rbn = new System.Windows.Forms.RadioButton();
      this.Option3_Rbn = new System.Windows.Forms.RadioButton();
      this.Option4_Rbn = new System.Windows.Forms.RadioButton();
      this.Option5_Rbn = new System.Windows.Forms.RadioButton();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.panel3 = new System.Windows.Forms.Panel();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // label2
      // 
      this.label2.BackColor = System.Drawing.Color.Azure;
      this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.label2.Font = new System.Drawing.Font("DX경필명조B", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.label2.Location = new System.Drawing.Point(12, 30);
      this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(201, 33);
      this.label2.TabIndex = 1;
      this.label2.Text = "검색어 입력";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // Search_Tbx
      // 
      this.Search_Tbx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Search_Tbx.Font = new System.Drawing.Font("DX경필명조B", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Search_Tbx.Location = new System.Drawing.Point(12, 70);
      this.Search_Tbx.Margin = new System.Windows.Forms.Padding(4);
      this.Search_Tbx.Multiline = true;
      this.Search_Tbx.Name = "Search_Tbx";
      this.Search_Tbx.Size = new System.Drawing.Size(201, 34);
      this.Search_Tbx.TabIndex = 2;
      // 
      // Post_Rbn
      // 
      this.Post_Rbn.AutoSize = true;
      this.Post_Rbn.Checked = true;
      this.Post_Rbn.Location = new System.Drawing.Point(20, 40);
      this.Post_Rbn.Name = "Post_Rbn";
      this.Post_Rbn.Size = new System.Drawing.Size(71, 20);
      this.Post_Rbn.TabIndex = 4;
      this.Post_Rbn.TabStop = true;
      this.Post_Rbn.Text = "포스트";
      this.Post_Rbn.UseVisualStyleBackColor = true;
      // 
      // Cafe_Rbn
      // 
      this.Cafe_Rbn.AutoSize = true;
      this.Cafe_Rbn.Font = new System.Drawing.Font("DX경필명조B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Cafe_Rbn.Location = new System.Drawing.Point(120, 40);
      this.Cafe_Rbn.Name = "Cafe_Rbn";
      this.Cafe_Rbn.Size = new System.Drawing.Size(56, 20);
      this.Cafe_Rbn.TabIndex = 5;
      this.Cafe_Rbn.Text = "카페";
      this.Cafe_Rbn.UseVisualStyleBackColor = true;
      // 
      // News_Rbn
      // 
      this.News_Rbn.AutoSize = true;
      this.News_Rbn.Font = new System.Drawing.Font("DX경필명조B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.News_Rbn.Location = new System.Drawing.Point(120, 80);
      this.News_Rbn.Name = "News_Rbn";
      this.News_Rbn.Size = new System.Drawing.Size(56, 20);
      this.News_Rbn.TabIndex = 7;
      this.News_Rbn.Text = "뉴스";
      this.News_Rbn.UseVisualStyleBackColor = true;
      // 
      // label4
      // 
      this.label4.BackColor = System.Drawing.Color.LavenderBlush;
      this.label4.Font = new System.Drawing.Font("DX경필명조B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.label4.Location = new System.Drawing.Point(235, 114);
      this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(583, 33);
      this.label4.TabIndex = 8;
      this.label4.Text = "구글 통합 검색 결과";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label5
      // 
      this.label5.BackColor = System.Drawing.Color.Lavender;
      this.label5.Font = new System.Drawing.Font("DX경필명조B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.label5.Location = new System.Drawing.Point(235, 461);
      this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(582, 40);
      this.label5.TabIndex = 11;
      this.label5.Text = "구글 동영상 검색 결과";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // Search_Lbx
      // 
      this.Search_Lbx.FormattingEnabled = true;
      this.Search_Lbx.ItemHeight = 16;
      this.Search_Lbx.Location = new System.Drawing.Point(234, 150);
      this.Search_Lbx.Name = "Search_Lbx";
      this.Search_Lbx.Size = new System.Drawing.Size(583, 308);
      this.Search_Lbx.TabIndex = 12;
      this.Search_Lbx.SelectedIndexChanged += new System.EventHandler(this.Search_Lbx_SelectedIndexChanged);
      // 
      // Video_Lbx
      // 
      this.Video_Lbx.FormattingEnabled = true;
      this.Video_Lbx.ItemHeight = 16;
      this.Video_Lbx.Location = new System.Drawing.Point(234, 506);
      this.Video_Lbx.Name = "Video_Lbx";
      this.Video_Lbx.Size = new System.Drawing.Size(583, 260);
      this.Video_Lbx.TabIndex = 13;
      this.Video_Lbx.SelectedIndexChanged += new System.EventHandler(this.Video_Lbx_SelectedIndexChanged);
      // 
      // Search_Btn
      // 
      this.Search_Btn.BackColor = System.Drawing.Color.PapayaWhip;
      this.Search_Btn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.Search_Btn.FlatAppearance.BorderColor = System.Drawing.Color.PapayaWhip;
      this.Search_Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.Search_Btn.Font = new System.Drawing.Font("DX경필명조B", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Search_Btn.Location = new System.Drawing.Point(1, 610);
      this.Search_Btn.Name = "Search_Btn";
      this.Search_Btn.Size = new System.Drawing.Size(231, 65);
      this.Search_Btn.TabIndex = 14;
      this.Search_Btn.Text = "검색 실행 (Enter)";
      this.Search_Btn.UseVisualStyleBackColor = false;
      this.Search_Btn.Click += new System.EventHandler(this.Search_Btn_Click);
      // 
      // label6
      // 
      this.label6.BackColor = System.Drawing.SystemColors.ControlLightLight;
      this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.label6.Font = new System.Drawing.Font("DX경필명조B", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.label6.Location = new System.Drawing.Point(825, 59);
      this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(586, 50);
      this.label6.TabIndex = 16;
      this.label6.Text = "네이버";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label7
      // 
      this.label7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.label7.Font = new System.Drawing.Font("DX경필명조B", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
      this.label7.Location = new System.Drawing.Point(235, 59);
      this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(582, 50);
      this.label7.TabIndex = 17;
      this.label7.Text = "구글";
      this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // Search_Lbx2
      // 
      this.Search_Lbx2.FormattingEnabled = true;
      this.Search_Lbx2.ItemHeight = 16;
      this.Search_Lbx2.Location = new System.Drawing.Point(825, 150);
      this.Search_Lbx2.Name = "Search_Lbx2";
      this.Search_Lbx2.Size = new System.Drawing.Size(586, 308);
      this.Search_Lbx2.TabIndex = 19;
      this.Search_Lbx2.SelectedIndexChanged += new System.EventHandler(this.Search_Lbx2_SelectedIndexChanged);
      // 
      // Video_Lbx2
      // 
      this.Video_Lbx2.FormattingEnabled = true;
      this.Video_Lbx2.ItemHeight = 16;
      this.Video_Lbx2.Location = new System.Drawing.Point(825, 506);
      this.Video_Lbx2.Name = "Video_Lbx2";
      this.Video_Lbx2.Size = new System.Drawing.Size(586, 260);
      this.Video_Lbx2.TabIndex = 21;
      this.Video_Lbx2.SelectedIndexChanged += new System.EventHandler(this.Video_Lbx2_SelectedIndexChanged);
      // 
      // Blog_Rbn
      // 
      this.Blog_Rbn.AutoSize = true;
      this.Blog_Rbn.Location = new System.Drawing.Point(20, 80);
      this.Blog_Rbn.Name = "Blog_Rbn";
      this.Blog_Rbn.Size = new System.Drawing.Size(71, 20);
      this.Blog_Rbn.TabIndex = 24;
      this.Blog_Rbn.Text = "블로그";
      this.Blog_Rbn.UseVisualStyleBackColor = true;
      // 
      // label9
      // 
      this.label9.BackColor = System.Drawing.Color.LavenderBlush;
      this.label9.Font = new System.Drawing.Font("DX경필명조B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.label9.Location = new System.Drawing.Point(825, 461);
      this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(586, 40);
      this.label9.TabIndex = 20;
      this.label9.Text = "네이버 동영상 검색 결과";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label8
      // 
      this.label8.BackColor = System.Drawing.Color.Lavender;
      this.label8.Font = new System.Drawing.Font("DX경필명조B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.label8.Location = new System.Drawing.Point(825, 114);
      this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(586, 33);
      this.label8.TabIndex = 18;
      this.label8.Text = "네이버 통합 검색 결과";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panel1
      // 
      this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panel1.Location = new System.Drawing.Point(234, 59);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(1, 717);
      this.panel1.TabIndex = 9;
      // 
      // panel2
      // 
      this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panel2.Location = new System.Drawing.Point(0, 59);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(1420, 1);
      this.panel2.TabIndex = 10;
      // 
      // label1
      // 
      this.label1.BackColor = System.Drawing.Color.SeaShell;
      this.label1.Dock = System.Windows.Forms.DockStyle.Top;
      this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
      this.label1.Font = new System.Drawing.Font("DX경필명조B", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(1411, 59);
      this.label1.TabIndex = 0;
      this.label1.Text = "Google / Naver Search";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // Option1_Rbn
      // 
      this.Option1_Rbn.AutoSize = true;
      this.Option1_Rbn.Font = new System.Drawing.Font("DX경필명조B", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Option1_Rbn.Location = new System.Drawing.Point(10, 150);
      this.Option1_Rbn.Name = "Option1_Rbn";
      this.Option1_Rbn.Size = new System.Drawing.Size(209, 19);
      this.Option1_Rbn.TabIndex = 27;
      this.Option1_Rbn.Text = "\"\" 검색 (검색어 반드시 포함)";
      this.Option1_Rbn.UseVisualStyleBackColor = true;
      // 
      // Option2_Rbn
      // 
      this.Option2_Rbn.AutoSize = true;
      this.Option2_Rbn.Font = new System.Drawing.Font("DX경필명조B", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Option2_Rbn.Location = new System.Drawing.Point(10, 60);
      this.Option2_Rbn.Name = "Option2_Rbn";
      this.Option2_Rbn.Size = new System.Drawing.Size(207, 19);
      this.Option2_Rbn.TabIndex = 28;
      this.Option2_Rbn.Text = "Intitle: (제목에 반드시 포함)";
      this.Option2_Rbn.UseVisualStyleBackColor = true;
      // 
      // Option3_Rbn
      // 
      this.Option3_Rbn.AutoSize = true;
      this.Option3_Rbn.Font = new System.Drawing.Font("DX경필명조B", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Option3_Rbn.Location = new System.Drawing.Point(10, 90);
      this.Option3_Rbn.Name = "Option3_Rbn";
      this.Option3_Rbn.Size = new System.Drawing.Size(206, 19);
      this.Option3_Rbn.TabIndex = 29;
      this.Option3_Rbn.Text = "Intext: (본문에 반드시 포함)";
      this.Option3_Rbn.UseVisualStyleBackColor = true;
      // 
      // Option4_Rbn
      // 
      this.Option4_Rbn.AutoSize = true;
      this.Option4_Rbn.Font = new System.Drawing.Font("DX경필명조B", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Option4_Rbn.Location = new System.Drawing.Point(10, 120);
      this.Option4_Rbn.Name = "Option4_Rbn";
      this.Option4_Rbn.Size = new System.Drawing.Size(171, 17);
      this.Option4_Rbn.TabIndex = 30;
      this.Option4_Rbn.Text = "Filetype: (EX : PDF java)";
      this.Option4_Rbn.UseVisualStyleBackColor = true;
      // 
      // Option5_Rbn
      // 
      this.Option5_Rbn.AutoSize = true;
      this.Option5_Rbn.Checked = true;
      this.Option5_Rbn.Font = new System.Drawing.Font("DX경필명조B", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Option5_Rbn.Location = new System.Drawing.Point(10, 30);
      this.Option5_Rbn.Name = "Option5_Rbn";
      this.Option5_Rbn.Size = new System.Drawing.Size(78, 17);
      this.Option5_Rbn.TabIndex = 31;
      this.Option5_Rbn.TabStop = true;
      this.Option5_Rbn.Text = "일반 검색";
      this.Option5_Rbn.UseVisualStyleBackColor = true;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.Post_Rbn);
      this.groupBox1.Controls.Add(this.Blog_Rbn);
      this.groupBox1.Controls.Add(this.Cafe_Rbn);
      this.groupBox1.Controls.Add(this.News_Rbn);
      this.groupBox1.ForeColor = System.Drawing.Color.MidnightBlue;
      this.groupBox1.Location = new System.Drawing.Point(5, 460);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(220, 120);
      this.groupBox1.TabIndex = 32;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "네이버 검색 종류";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.Option5_Rbn);
      this.groupBox2.Controls.Add(this.Option2_Rbn);
      this.groupBox2.Controls.Add(this.Option1_Rbn);
      this.groupBox2.Controls.Add(this.Option4_Rbn);
      this.groupBox2.Controls.Add(this.Option3_Rbn);
      this.groupBox2.ForeColor = System.Drawing.Color.Crimson;
      this.groupBox2.Location = new System.Drawing.Point(5, 220);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(220, 195);
      this.groupBox2.TabIndex = 33;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "구글 검색 연산자";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.label2);
      this.groupBox3.Controls.Add(this.Search_Tbx);
      this.groupBox3.ForeColor = System.Drawing.Color.BlueViolet;
      this.groupBox3.Location = new System.Drawing.Point(5, 66);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(220, 122);
      this.groupBox3.TabIndex = 34;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "검색 옵션";
      // 
      // panel3
      // 
      this.panel3.BackColor = System.Drawing.SystemColors.Desktop;
      this.panel3.Location = new System.Drawing.Point(20, 670);
      this.panel3.Name = "panel3";
      this.panel3.Size = new System.Drawing.Size(190, 1);
      this.panel3.TabIndex = 35;
      // 
      // Form1
      // 
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
      this.AutoSize = true;
      this.BackColor = System.Drawing.Color.PapayaWhip;
      this.ClientSize = new System.Drawing.Size(1411, 766);
      this.Controls.Add(this.panel3);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.Video_Lbx2);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.Search_Lbx2);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.Search_Btn);
      this.Controls.Add(this.Video_Lbx);
      this.Controls.Add(this.Search_Lbx);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label1);
      this.Font = new System.Drawing.Font("DX경필명조B", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
      this.Margin = new System.Windows.Forms.Padding(4);
      this.Name = "Form1";
      this.Text = "GoogleSearch (Created by Trekker)";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);

    }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Search_Tbx;
        private System.Windows.Forms.RadioButton Post_Rbn;
        private System.Windows.Forms.RadioButton Cafe_Rbn;
        private System.Windows.Forms.RadioButton News_Rbn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox Search_Lbx;
        private System.Windows.Forms.ListBox Video_Lbx;
        private System.Windows.Forms.Button Search_Btn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox Search_Lbx2;
        private System.Windows.Forms.ListBox Video_Lbx2;
        private System.Windows.Forms.RadioButton Blog_Rbn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton Option1_Rbn;
        private System.Windows.Forms.RadioButton Option2_Rbn;
        private System.Windows.Forms.RadioButton Option3_Rbn;
        private System.Windows.Forms.RadioButton Option4_Rbn;
        private System.Windows.Forms.RadioButton Option5_Rbn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel3;
    }
}

